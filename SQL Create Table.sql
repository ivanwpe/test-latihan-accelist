create DATABASE BinusLuna

create table Role(
	RoleId int identity(1,1) PRIMARY KEY,
	Name varchar(255) NOT NULL,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Users(
	UserId int identity(1,1) PRIMARY KEY,
	RoleId int FOREIGN KEY REFERENCES Role(RoleId),
	Name varchar(255) NOT NULL,
    NIM VARCHAR(10) NOT NULL,
	Email varchar(255) NOT NULL UNIQUE,
	Password varchar(255) NOT NULL,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Mail(
    MailId int identity(1,1) PRIMARY KEY,
    UserId int FOREIGN KEY REFERENCES Users(UserId),
    MailTitle varchar(255) NOT NULL,
    CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table CoursesCategory(
    CoursesCategoryId int identity(1,1) PRIMARY KEY,
	CoursesCategoryName varchar(255) NOT NULL,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table CoursesDetail(
    CourseId int identity(1,1) PRIMARY KEY,
    CoursesCategoryId int FOREIGN KEY REFERENCES CoursesCategory(CoursesCategoryId),
    CourseName varchar(255) NOT NULL,
    CourseSemester varchar(255) NOT NULL,
    CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table ForumCategory(
    ForumCategoryId int identity(1,1) PRIMARY KEY,
	ForumCategoryName varchar(255) NOT NULL,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table ForumHeader(
    ForumId int identity(1,1) PRIMARY KEY,
	ForumCategoryId int FOREIGN KEY REFERENCES ForumCategory(ForumCategoryId),
	ForumName varchar(255) NOT NULL,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table ForumDetail(
    ForumDetailId int identity(1,1) PRIMARY KEY,
	ForumId int FOREIGN KEY REFERENCES ForumHeader(ForumId),
	UserId int FOREIGN KEY REFERENCES Users(UserId),
    CourseId int FOREIGN KEY REFERENCES CoursesDetail(CourseId),
	Content varchar(255) NOT NULL,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table FinancialCategory(
    FinancialCategoryId int identity(1,1) PRIMARY KEY,
    FinancialCategoryName varchar(255) NOT NULL,
    CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table FinancialDetail(
    FinancialId int identity(1,1) PRIMARY KEY,
    FinancialCategoryId int FOREIGN KEY REFERENCES FinancialCategory(FinancialCategoryId),
    FinancialSummary varchar(255) NOT NULL,
    PaymentReceipt varchar(255),
    CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table LearningCategory(
    LearningCategoryId int identity(1,1) PRIMARY KEY,
    LearningCategoryName varchar(255) NOT NULL,
    CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Schedule(
    ScheduleId int identity(1,1) PRIMARY KEY,
    LearningCategoryId int FOREIGN KEY REFERENCES LearningCategory(LearningCategoryId),
    CourseId int FOREIGN KEY REFERENCES CoursesDetail(CourseId),
    ScheduleDate DATE NOT NULL,
    GSLC BIT,
    CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Grades(
    ScoreId int identity(1,1) PRIMARY KEY,
    LearningCategoryId int FOREIGN KEY REFERENCES LearningCategory(LearningCategoryId),
    UserId int FOREIGN KEY REFERENCES Users(UserId),
    CourseId int FOREIGN KEY REFERENCES CoursesDetail(CourseId),
    Score varchar(3) DEFAULT NULL,
    CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)
